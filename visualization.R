library(readr)
Crime_girls <- read_csv("Crime_girls.csv",show_col_types = FALSE)
pdf("visualization.pdf",width=10,height=7)
par(mar=c(11,9,4,4))
barplot(Crime_girls$Total,names=Crime_girls$State,main =" Total crimes in India ",ylab="crime rate",col = "BLACK",las=2)
x<-Crime_girls$`MURDER OF CHILDREN`
l<-Crime_girls$State
piepercent<- round(100*x/sum(x), 1)
par(mar=c(5,5,5,0))
pie(x,piepercent,font=2, density = NULL, lty = NULL, border = NULL,main ="Crime-Rate(Murder) In Different States of India",col = rainbow(length(x)),)
legend(x = "topright",l, cex = 0.8, fill = rainbow(length(x)))
dev.off()
